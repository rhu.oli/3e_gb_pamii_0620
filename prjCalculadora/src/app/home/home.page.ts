import { Component } from '@angular/core';
import { calculadora } from '../models/calculadora';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  calculadora: calculadora;
  resp: number;

  constructor() {
    this.calculadora = new calculadora();
  }

  private calcular(operacao: string){
    this.calculadora.operacao = operacao;
    this.resp = this.calculadora.calcular();
  }


}
